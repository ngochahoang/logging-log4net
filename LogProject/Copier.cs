﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProject
{
    class Copier
    {
        public static bool copyFolder(bool identifyTime,string sourceUrl,string destinationUrl)
        {
            string directoryName = " ";
            string path = " ";
            bool result = true;
            if (identifyTime)
            {
                directoryName = FormatDirectoryName("cp");
                path = GetFolderPath(destinationUrl, directoryName);
             //  path = Path.Combine(destinationUrl, directoryName);

            }
            else
            {
                directoryName = FormatDirectoryName("cpRealTime");
                path = GetFolderPath(destinationUrl, directoryName);
            //    path = Path.Combine(destinationUrl, directoryName);
            }
            try
            {
                CreateDirectoryExist(path);
                CopyDirectory(sourceUrl, path, true);
            }
            catch (Exception e)
            {
                result = false;
                Console.WriteLine("Error: " + e.Message);
            }
            return result;
        }

        private static void CreateDirectoryExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        private static void CopyDirectory(string sourceDirName,string destDirName,bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    if (!Directory.Exists(temppath))
                    {
                        Directory.CreateDirectory(temppath);
                    }
                    CopyDirectory(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
  

        public static string FormatDirectoryName(string name)
        {
            string directoryName = name + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            return directoryName;
        }

        public static string GetFolderPath(string pathFolder, string pathSubFolder)
        {
            string path = Path.Combine(pathFolder, pathSubFolder);
            return path;
        }
    }
}
