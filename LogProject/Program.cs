﻿using log4net;
using System;
using System.Threading;
using System.Windows.Forms;
using LogProject.CronNet;
using System.Xml;

namespace LogProject
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly CronDaemon cronDaemon = new CronDaemon();
        private static string xmlFile = "settingtime.xml";
        private static string settingType = " ";
        private static string settingData = " ";
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (loadXmlFile())
            {
               string expression = SettingTimeForm.setSchedule(settingType, settingData);
               cronDaemon.AddJob(expression, task);
               cronDaemon.Start();
                MessageBox.Show("Start : " +expression);
            }
            Application.Run(new Form1());
            log.Info("Start using application");
           
        }
       
        private static void task()
        {
            MessageBox.Show("New job");
        }

        private static bool loadXmlFile()
        {
            bool output = true;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode root = doc.DocumentElement;
            XmlNode type = root.SelectSingleNode("/settingtime/type");
            XmlNode data = root.SelectSingleNode("/settingtime/data");
            if(type.InnerText.Length == 0 || data.InnerText.Length == 0)
            {
                output = false;
            }
            else
            {
                settingType = type.InnerText;
                settingData = data.InnerText;
            }
            return output;
        }

       

      
      
    }
}
