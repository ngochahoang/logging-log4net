﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LogProject
{
    public partial class FolderForm : Form
    {
        private string xmlFile = "settingtime.xml";
        public FolderForm()
        {
            InitializeComponent();
            settingOriginal();
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string sourceUrl = tbxSourceUrl.Text;
            string destinationUrl = tbxDestinationUrl.Text;
            if (ValidateForm(sourceUrl, destinationUrl))
            {
                //save it to file xml
                saveXmlFile(sourceUrl, destinationUrl);
            }
        }

        private void saveXmlFile(string source, string destination)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode root = doc.DocumentElement;
            XmlNode sourceUrl = root.SelectSingleNode("/settingtime/sourceUrl");
            XmlNode destinationUrl = root.SelectSingleNode("/settingtime/destinationUrl");
            sourceUrl.InnerText = source;
            destinationUrl.InnerText = destination;
            doc.Save(xmlFile);
        }

        private bool ValidateForm(string sourceUrl,string destinationUrl)
        {
            bool output = true;
            if(sourceUrl.Length == 0 || destinationUrl.Length == 0)
            {
                output = false;
                MessageBox.Show("SourceUrl or DestinationUrl is required","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                
            }

            return output;
        }

        private void BtnChooseFolder_Click(object sender, EventArgs e)
        {
            setupChoosefolder(tbxSourceUrl);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            setupChoosefolder(tbxDestinationUrl);
        
        }
        private void setupChoosefolder(TextBox tbxUrl)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = true;
            DialogResult result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                tbxUrl.Text = folderBrowser.SelectedPath;
                Environment.SpecialFolder root = folderBrowser.RootFolder;
            }
        }
        
        private void settingOriginal()
        {
            tbxSourceUrl.Enabled = false;
            tbxDestinationUrl.Enabled = false;
        }
    }
}
