﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LogProject
{
    public partial class SettingTimeForm : Form
    {
        private const string DAY_OF_WEEK = "Day of week"; // 0-6 0:sunday,1:monday,2:tuesday,...
        private const string DAY_OF_MONTH = "Day of month";// 1->31
        private const string HOUR_OF_DAY = "Hour of day";
        private const string DEFAUL_VALUE = "Select Option";
        private string xmlFile = "settingtime.xml";

       // private string[] dayOfWeek = new string[] { DEFAUL_VALUE, "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Satuday", "Sunday" }; //Weekdays
        private string[] settingType = new string[] { DEFAUL_VALUE, DAY_OF_WEEK,DAY_OF_MONTH, HOUR_OF_DAY };
        public SettingTimeForm()
        {
            InitializeComponent();
            setupComboboxSettingType();

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode root = doc.DocumentElement;
            XmlNode settingType = root.SelectSingleNode("/settingtime/type");
            XmlNode settingData = root.SelectSingleNode("/settingtime/data");          
            int index = cbbsettingType.Items.IndexOf(settingType.InnerText);
            cbbsettingType.SelectedIndex = index;
            tbxData.Text = settingData.InnerText;

        }
      
        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string settingType = ((Object)cbbsettingType.SelectedItem).ToString();
         //   Console.WriteLine("{0}", index);
           if(tbxData.Text.Length != 0)
            {
                //save data to xmlfile
                saveXmlFile(settingType, tbxData.Text);
                string expression = setSchedule(settingType, tbxData.Text);
                Console.WriteLine("Expression = " + expression);
            }
          

        }

        //Save all data from form to file settingtime.xml
        private void saveXmlFile(string type,string data)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode root = doc.DocumentElement;
            XmlNode settingType = root.SelectSingleNode("/settingtime/type");
            XmlNode settingData = root.SelectSingleNode("/settingtime/data");
            settingType.InnerText = type;
            settingData.InnerText = data;
            doc.Save(xmlFile);
        }

        private void setupComboboxSettingType()
        {
            cbbsettingType.BeginUpdate();
            cbbsettingType.DataSource = settingType;
            cbbsettingType.EndUpdate();         
        }

     
        private void TbtTimes_TextChanged(object sender, EventArgs e)
        {

        }
  
        private void CbbsettingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxData.Text = "";
            int selected = cbbsettingType.SelectedIndex;
            Object selectedItem = cbbsettingType.SelectedItem;
            string settingType = selectedItem.ToString();
           //Set valid data for each setting type
            label3.Text = setValidData(settingType);

        }

        private string setValidData(string settingType)
        {
            string validData = " ";
            if (settingType == DAY_OF_WEEK)
            {
                validData = "0-6 with 0 from Sunday";
            }
            if (settingType == DAY_OF_MONTH)
            {
                validData = "1-31";
            }
            if (settingType == HOUR_OF_DAY)
            {
                validData = "0-24";
            }
            return validData;
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {

        }

     
        public static string setSchedule(string settingType,string data)
        {
            string expression = " ";
            if(settingType == DAY_OF_WEEK) //0-6
            {
                expression = "0 0 * * " + data;
            }
            if(settingType == DAY_OF_MONTH) //1-31
            {
                expression = "0 17 " + data + " * *";
            }
            if(settingType == HOUR_OF_DAY)//number
            {
                //only one character
                expression = "* */" + data + " * * *";
            }
            return expression;
            
        }
    }
}
