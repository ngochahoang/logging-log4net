﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LogProject
{
    public partial class Form1 : Form
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string sourceUrl = " ";
        private string destinationUrl = " ";
        private string xmlFile = "settingtime.xml";
        public Form1()
        {
            InitializeComponent();
            logger.Info("Loaded login page");
        }

        private void BtnSettingTime_Click(object sender, EventArgs e)
        {
            Form settingTimeForm = new SettingTimeForm();
            settingTimeForm.Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
             XmlDocument doc = new XmlDocument();
             doc.Load("settingtime.xml");
             XmlNode type = doc.DocumentElement.SelectSingleNode("/settingtime/type");
             Console.WriteLine("Type node" + type.InnerText);
            

        }

        private void BtnRealTimeCopy_Click(object sender, EventArgs e)
        {
            loadXmlFile();
            if (Copier.copyFolder(false,sourceUrl,destinationUrl))
            {
                Console.WriteLine("Copied");
            }
            else
            {
                Console.WriteLine("Can't copied");
            }
        }

        private void loadXmlFile()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode root = doc.DocumentElement;
            XmlNode url1 = root.SelectSingleNode("/settingtime/sourceUrl");
            XmlNode url2 = root.SelectSingleNode("/settingtime/destinationUrl");
            sourceUrl = url1.InnerText;
            destinationUrl = url2.InnerText;
        }

        private void BtnSettingFolder_Click(object sender, EventArgs e)
        {
            Form chooseFolder = new FolderForm();
            chooseFolder.Show();
        }
    }
}
