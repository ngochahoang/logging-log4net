﻿namespace LogProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSettingTime = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnRealTimeCopy = new System.Windows.Forms.Button();
            this.btnSettingFolder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSettingTime
            // 
            this.btnSettingTime.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSettingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettingTime.Location = new System.Drawing.Point(287, 67);
            this.btnSettingTime.Name = "btnSettingTime";
            this.btnSettingTime.Size = new System.Drawing.Size(188, 78);
            this.btnSettingTime.TabIndex = 5;
            this.btnSettingTime.Text = "Setting Time ";
            this.btnSettingTime.UseVisualStyleBackColor = false;
            this.btnSettingTime.Click += new System.EventHandler(this.BtnSettingTime_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Show XML File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnRealTimeCopy
            // 
            this.btnRealTimeCopy.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnRealTimeCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealTimeCopy.Location = new System.Drawing.Point(287, 190);
            this.btnRealTimeCopy.Name = "btnRealTimeCopy";
            this.btnRealTimeCopy.Size = new System.Drawing.Size(188, 78);
            this.btnRealTimeCopy.TabIndex = 7;
            this.btnRealTimeCopy.Text = "Real-time Copy ";
            this.btnRealTimeCopy.UseVisualStyleBackColor = false;
            this.btnRealTimeCopy.Click += new System.EventHandler(this.BtnRealTimeCopy_Click);
            // 
            // btnSettingFolder
            // 
            this.btnSettingFolder.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSettingFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettingFolder.Location = new System.Drawing.Point(287, 311);
            this.btnSettingFolder.Name = "btnSettingFolder";
            this.btnSettingFolder.Size = new System.Drawing.Size(188, 78);
            this.btnSettingFolder.TabIndex = 8;
            this.btnSettingFolder.Text = "Setting Folder";
            this.btnSettingFolder.UseVisualStyleBackColor = false;
            this.btnSettingFolder.Click += new System.EventHandler(this.BtnSettingFolder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSettingFolder);
            this.Controls.Add(this.btnRealTimeCopy);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSettingTime);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSettingTime;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRealTimeCopy;
        private System.Windows.Forms.Button btnSettingFolder;
    }
}

