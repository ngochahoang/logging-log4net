﻿namespace LogProject
{
    partial class SettingTimeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxData = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbsettingType = new System.Windows.Forms.ComboBox();
            this.btnSettingTime = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(288, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "SETTING TIME";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnSettingTime);
            this.panel1.Location = new System.Drawing.Point(36, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(730, 334);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.tbxData);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cbbsettingType);
            this.panel2.Location = new System.Drawing.Point(30, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(469, 235);
            this.panel2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(211, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "label3";
            // 
            // tbxData
            // 
            this.tbxData.Location = new System.Drawing.Point(214, 123);
            this.tbxData.Name = "tbxData";
            this.tbxData.Size = new System.Drawing.Size(210, 20);
            this.tbxData.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Setting Time Type";
            // 
            // cbbsettingType
            // 
            this.cbbsettingType.FormattingEnabled = true;
            this.cbbsettingType.Location = new System.Drawing.Point(214, 41);
            this.cbbsettingType.Name = "cbbsettingType";
            this.cbbsettingType.Size = new System.Drawing.Size(210, 21);
            this.cbbsettingType.TabIndex = 8;
            this.cbbsettingType.SelectedIndexChanged += new System.EventHandler(this.CbbsettingType_SelectedIndexChanged);
            // 
            // btnSettingTime
            // 
            this.btnSettingTime.Location = new System.Drawing.Point(588, 153);
            this.btnSettingTime.Name = "btnSettingTime";
            this.btnSettingTime.Size = new System.Drawing.Size(90, 31);
            this.btnSettingTime.TabIndex = 6;
            this.btnSettingTime.Text = "Setting Time";
            this.btnSettingTime.UseVisualStyleBackColor = true;
            this.btnSettingTime.Click += new System.EventHandler(this.Button1_Click);
            // 
            // SettingTimeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "SettingTimeForm";
            this.Text = "SettingTimeForm";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSettingTime;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbbsettingType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxData;
        private System.Windows.Forms.Label label3;
    }
}