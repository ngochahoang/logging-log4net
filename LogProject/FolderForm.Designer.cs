﻿namespace LogProject
{
    partial class FolderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxDestinationUrl = new System.Windows.Forms.TextBox();
            this.btnChooseFolder = new System.Windows.Forms.Button();
            this.tbxSourceUrl = new System.Windows.Forms.TextBox();
            this.btnChooseDestinateUrl = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(190, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(356, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "CHOOSE FOLDER TO COPY";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(99, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Source URL";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(332, 344);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(106, 47);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(99, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Destination URL";
            this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // tbxDestinationUrl
            // 
            this.tbxDestinationUrl.Location = new System.Drawing.Point(296, 159);
            this.tbxDestinationUrl.Name = "tbxDestinationUrl";
            this.tbxDestinationUrl.Size = new System.Drawing.Size(340, 20);
            this.tbxDestinationUrl.TabIndex = 5;
            // 
            // btnChooseFolder
            // 
            this.btnChooseFolder.Location = new System.Drawing.Point(656, 243);
            this.btnChooseFolder.Name = "btnChooseFolder";
            this.btnChooseFolder.Size = new System.Drawing.Size(87, 33);
            this.btnChooseFolder.TabIndex = 6;
            this.btnChooseFolder.Text = "Choose folder";
            this.btnChooseFolder.UseVisualStyleBackColor = true;
            this.btnChooseFolder.Click += new System.EventHandler(this.BtnChooseFolder_Click);
            // 
            // tbxSourceUrl
            // 
            this.tbxSourceUrl.Location = new System.Drawing.Point(296, 249);
            this.tbxSourceUrl.Name = "tbxSourceUrl";
            this.tbxSourceUrl.Size = new System.Drawing.Size(340, 20);
            this.tbxSourceUrl.TabIndex = 7;
            // 
            // btnChooseDestinateUrl
            // 
            this.btnChooseDestinateUrl.Location = new System.Drawing.Point(656, 159);
            this.btnChooseDestinateUrl.Name = "btnChooseDestinateUrl";
            this.btnChooseDestinateUrl.Size = new System.Drawing.Size(87, 33);
            this.btnChooseDestinateUrl.TabIndex = 8;
            this.btnChooseDestinateUrl.Text = "Choose folder";
            this.btnChooseDestinateUrl.UseVisualStyleBackColor = true;
            this.btnChooseDestinateUrl.Click += new System.EventHandler(this.Button1_Click);
            // 
            // FolderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnChooseDestinateUrl);
            this.Controls.Add(this.tbxSourceUrl);
            this.Controls.Add(this.btnChooseFolder);
            this.Controls.Add(this.tbxDestinationUrl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FolderForm";
            this.Text = "ChooseFolder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxDestinationUrl;
        private System.Windows.Forms.Button btnChooseFolder;
        private System.Windows.Forms.TextBox tbxSourceUrl;
        private System.Windows.Forms.Button btnChooseDestinateUrl;
    }
}